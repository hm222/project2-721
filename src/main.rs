use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, HttpRequest, error::ResponseError, http::StatusCode, Error};
use serde_json::json;
use aws_sdk_dynamodb::{Client, Region, model::AttributeValue};
use aws_config::meta::region::RegionProviderChain;
use serde::Deserialize;
use std::fmt;

#[derive(Deserialize, Debug)]
struct DynamoRequest {
    table_name: String,
    employer: String,
    email: String,
}

#[derive(Debug)]
enum UserError {
    DynamoDbError(String),
    BadRequest(String),
}

impl fmt::Display for UserError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            UserError::DynamoDbError(ref message) => write!(f, "DynamoDB Error: {}", message),
            UserError::BadRequest(ref message) => write!(f, "Bad Request: {}", message),
        }
    }
}

impl ResponseError for UserError {
    fn error_response(&self) -> HttpResponse {
        match *self {
            UserError::DynamoDbError(_) => {
                let error_message = json!({ "error": self.to_string() });
                HttpResponse::InternalServerError().json(error_message)
            },
            UserError::BadRequest(_) => {
                let error_message = json!({ "error": self.to_string() });
                HttpResponse::BadRequest().json(error_message)
            },
        }
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            UserError::DynamoDbError(_) => StatusCode::INTERNAL_SERVER_ERROR,
            UserError::BadRequest(_) => StatusCode::BAD_REQUEST,
        }
    }
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body("<h1>Hello World!</h1><p>Welcome to our Rust Actix web app.</p>")
}

#[post("/echo")]
async fn echo(req_body: String) -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(format!("<h2>Echo</h2><p>{}</p>", req_body))
}

#[get("/micro-dynamo")]
async fn update_db(req: HttpRequest) -> Result<HttpResponse, Error> {
    let region_provider = RegionProviderChain::default_provider().or_else(Region::new("us-east-2"));
    let config = aws_config::from_env().region(region_provider).load().await;
    let client = Client::new(&config);

    let dynamo_req = web::Query::<DynamoRequest>::from_query(req.query_string())
        .map_err(|_| UserError::BadRequest("Invalid query string".to_string()))?; 

    let mut item = std::collections::HashMap::new();
    item.insert("Employer".to_string(), AttributeValue::S(dynamo_req.employer.clone()));
    item.insert("Value".to_string(), AttributeValue::S(dynamo_req.email.clone()));

    client.put_item()
        .table_name(&dynamo_req.table_name)
        .set_item(Some(item))
        .send().await
        .map_err(|e| UserError::DynamoDbError(format!("DynamoDB error: {:?}", e)))?;

    Ok(HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(format!("<h4>Item Below Inserted Successfully!</h4><p>{:?}</p>", dynamo_req)))
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body("<h1>Hey there!</h1><p>This is a manually defined route.</p>")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .service(update_db)
            .route("/hey", web::get().to(manual_hello))
    })
    .bind(("0.0.0.0", 8080))?
    .run()
    .await
}
