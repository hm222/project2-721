# Use the official Rust image as the base image
FROM rustlang/rust:nightly as builder

# Create a new empty shell project
RUN USER=root cargo new --bin project2
WORKDIR /project2

# Copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# Copy your source tree
COPY ./src ./src

# Build for release.
# We're doing a touch on all .rs files because if there are no changes in the files,
# Docker will use the cache and won't rebuild our application. This is a way to ensure
# that our application is actually rebuilt.
RUN touch src/*.rs && cargo build --release

# The final image will be based on Debian buster
FROM debian:stable

# Install OpenSSL - it's dynamically linked by many Rust applications
RUN apt-get update && apt-get install -y libssl-dev && rm -rf /var/lib/apt/lists/*

# Copy the binary from the builder stage
COPY --from=builder /project2/target/release/project2 .

# Set the startup command to run your binary
CMD ["./project2"]
