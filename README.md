# Project 2

This repository contains the source code for a web application built with Actix. The application has been containerized using Docker for easy deployment and scalability.

## Overview

The Actix web service provides several routes that will respond with different html responses, and a specific route accepting http get requests to update a DynamoDB table. This project is set up to run within a Docker container, ensuring a consistent and isolated environment across different stages of development, testing, and production.

## Prerequisites

Before you begin, ensure you have the following installed on your system:
- Docker
- Rust (if you plan to develop or build locally)
- AWS Credentials (if you plan to run this container locally)
- AWS DynamoDB with table "test", and columns "employer" + "email"

## Building the Docker Image

To build the Docker image for the Actix web service, navigate to the root directory of this project and run:

```bash
docker build -t project2 .
```

This command builds a Docker image named `project2` based on the instructions in the `Dockerfile`. 

## Running the Container

After building the image, you can run the Actix web service in a Docker container using:

```bash
docker run -d -p 8080:8080 project2
```

This command runs the `project2` container in detached mode (`-d`) and maps port 8080 of the container to port 8080 on the host, allowing you to access the web service at `http://localhost:8080`.

The following screenshots show that the dynamo route is working:

### Specific DynamoDB Route
![DynamoDB Route After Successful Insertion/Updates](docker-dynamo-route.png)

### Updated Item in DynamoDB
![DynamoDB Table After Successful Updates](dynamo-aws.png)

## Customization

- To customize the application, you can modify the source code in the `src` directory. After making changes, rebuild the Docker image to apply them.
- For specific configurations related to Actix or Docker, refer to the `main.rs` file or the `Dockerfile`, respectively.
- To use the specific DynamoDB route, type the following url in the browser:

[http://localhost:8080/micro-dynamo?table_name=test&employer=mike&email=mmm](http://localhost:8080/micro-dynamo?table_name=test&employer=mike&email=mmkk)

Replace the `employer` and `email` query strings with your own values to customize the database entry. If the command succeeds, the website will show the successful message and the updated item in the html response.